/*----------------------------------------------------------------------------*/
/**
 * @ingroup MclCore
 * @file    platform.hpp
 * @brief   Header file (C++) for MCL platform configuration.
 *
 * @author  Robson Martins (http://www.robsonmartins.com)
 */
/*----------------------------------------------------------------------------*/

#ifndef LIB_MCL_PLATFORM_HPP_
#define LIB_MCL_PLATFORM_HPP_ 1

/*----------------------------------------------------------------------------*/
/* config compiler macros */
/*----------------------------------------------------------------------------*/
/** @defgroup MclCoreConstCompiler Compiler Constants
    @ingroup  MclCoreConst
    @brief    Compiler Constants */
/*----------------------------------------------------------------------------*/
/** @addtogroup MclCoreConstCompiler
 *  @{ */
/** @name Compiler Constants
 *  @{  */
#ifdef DOCUMENTATION
/** @def CC_MSVC
 *  @brief Microsoft Visual C Compiler */
#  define CC_MSVC
/** @def CC_MINGW
 *  @brief MinGW (GCC/Windows) Compiler */
# define CC_MINGW
/** @def CC_GCC
 *  @brief GNU C Compiler */
# define CC_GCC
/** @def CC_CLANG
 *  @brief CLang Compiler */
# define CC_CLANG
/** @def CC_UNKNOWN
 *  @brief Unknown Compiler */
# define CC_UNKNOWN
#endif
/** @} */
/** @} */

/*----------------------------------------------------------------------------*/

/** @cond SKIP_DOCUMENTATION */
#if defined(_MSC_VER)
#  define CC_MSVC
#elif defined(__MINGW32__)
#  define CC_MINGW
#elif defined(__GNUC__)
# define CC_GCC
#elif defined(__clang__)
# define CC_CLANG
#else
# define CC_UNKNOWN
#endif
/** @endcond */

/*----------------------------------------------------------------------------*/
/* config OS macros, based in compiler flags */
/*----------------------------------------------------------------------------*/
/** @defgroup MclCoreConstOS Operational System Constants
    @ingroup  MclCoreConst
    @brief    Operational System Constants */
/*----------------------------------------------------------------------------*/
/** @addtogroup MclCoreConstOS
 *  @{ */
/** @name Operational System Constants
 *  @{  */
#ifdef DOCUMENTATION
/** @def OS_UNIX
 *  @brief UNIX/POSIX Compatible Operational System */
# define OS_UNIX
/** @def OS_WINDOWS
 *  @brief Microsoft Windows Operational System */
# define OS_WINDOWS
/** @def OS_BSD
 *  @brief BSD UNIX Compatible Operational System */
# define OS_BSD
/** @def OS_FREEBSD
 *  @brief FreeBSD Operational System */
# define OS_FREEBSD
/** @def OS_NETBSD
 *  @brief NetBSD Operational System */
# define OS_NETBSD
/** @def OS_OPENBSD
 *  @brief OpenBSD Operational System */
# define OS_OPENBSD
/** @def OS_LINUX
 *  @brief GNU/Linux Operational System */
# define OS_LINUX
/** @def OS_SOLARIS
 *  @brief SUN/Solaris Operational System */
# define OS_SOLARIS
/** @deprecated Use @ref OS_MACOSX instead.
 *  @def OS_MACOS
 *  @brief MacOSX/Darwin Operational System */
# define OS_MACOS
/** @def OS_MACOSX
 *  @brief MacOSX/Darwin Operational System */
# define OS_MACOSX
#endif
/** @} */
/** @} */

/*----------------------------------------------------------------------------*/
/** @defgroup MclCoreConstOSArch Operational System Architecture Constants
    @ingroup  MclCoreConst
    @brief    Operational System Architecture Constants */
/*----------------------------------------------------------------------------*/
/** @addtogroup MclCoreConstOSArch
 *  @{ */
/** @name Operational System Architecture Constants
 *  @{  */
#ifdef DOCUMENTATION
/** @def OS_IS32BITS
 *  @brief 32 Bits Operational System. */
# define OS_IS32BITS
/** @def OS_IS64BITS
 *  @brief 64 Bits Operational System. */
# define OS_IS64BITS
#endif
/** @} */
/** @} */

/*----------------------------------------------------------------------------*/
/** @defgroup MclCoreConstUtility Utility Constants
    @ingroup  MclCoreConst
    @brief    Utility Constants */
/*----------------------------------------------------------------------------*/
/** @addtogroup MclCoreConstUtility
 *  @{ */
/** @name Utility Constants
 *  @{  */
#ifdef DOCUMENTATION
/** @def DLIMPORT
 *  @brief Dynamic Library Import Declaration */
# define DLIMPORT
/** @deprecated Use @ref DLIMPORT instead.
 *  @def DLLIMPORT
 *  @brief Dynamic Library Import Declaration */
# define DLLIMPORT
/** @def DLEXPORT
 *  @brief Dynamic Library Export Declaration */
# define DLEXPORT
/** @deprecated Use @ref DLEXPORT instead.
 *  @def DLLEXPORT
 *  @brief Dynamic Library Export Declaration */
# define DLLEXPORT
#endif
/** @} */
/** @} */

/*----------------------------------------------------------------------------*/

/** @cond SKIP_DOCUMENTATION */
#if defined(CC_GCC) || defined(CC_MINGW) || defined(CC_CLANG)
  /* GCC/MinGW32/CLang */
  /*--------------------------------------------------------------------------*/
# if defined(linux)   || defined(__linux__) ||\
     defined(__linux) || defined(__gnu_linux__)
   /* GNU/Linux */
#  if !defined(OS_LINUX)
#   define OS_LINUX
#  endif
#  if !defined(OS_UNIX)
#   define OS_UNIX
#  endif
  /*--------------------------------------------------------------------------*/
# elif defined(WIN32) || defined(__WIN32__) || defined(__WIN32) ||\
       defined(WINNT) || defined(__WINNT__) || defined(__WINNT) ||\
       defined(CC_MINGW)
   /* Microsoft Windows */
#  if !defined(OS_WINDOWS)
#   define OS_WINDOWS
#  endif
#  if defined(OS_UNIX)
#   undef OS_UNIX
#  endif
  /*--------------------------------------------------------------------------*/
# elif defined(__FreeBSD__) || defined(__freebsd__) || defined(__FREEBSD__) ||\
       defined(__FreeBSD_kernel__)
   /* FreeBSD Unix (or Debian kFreeBSD)*/
#  if !defined(OS_FREEBSD)
#   define OS_FREEBSD
#  endif
#  if !defined(OS_BSD)
#   define OS_BSD
#  endif
#  if !defined(OS_UNIX)
#   define OS_UNIX
#  endif
  /*--------------------------------------------------------------------------*/
# elif defined(__NetBSD__)  || defined(__netbsd__)  || defined(__NETBSD__)
   /* NetBSD Unix */
#  if !defined(OS_NETBSD)
#   define OS_NETBSD
#  endif
#  if !defined(OS_BSD)
#   define OS_BSD
#  endif
#  if !defined(OS_UNIX)
#   define OS_UNIX
#  endif
  /*--------------------------------------------------------------------------*/
# elif defined(__OpenBSD__) || defined(__openbsd__) || defined(__OPENBSD__)
   /* OpenBSD Unix */
#  if !defined(OS_OPENBSD)
#   define OS_OPENBSD
#  endif
#  if !defined(OS_BSD)
#   define OS_BSD
#  endif
#  if !defined(OS_UNIX)
#   define OS_UNIX
#  endif
  /*--------------------------------------------------------------------------*/
# elif defined(sun)   || defined(__sun__)   || defined(__sun) ||\
       defined(sunos) || defined(__sunos__) || defined(__sunos)
   /* Sun Solaris */
#  if !defined(OS_SOLARIS)
#   define OS_SOLARIS
#  endif
#  if !defined(OS_UNIX)
#   define OS_UNIX
#  endif
  /*--------------------------------------------------------------------------*/
# elif defined(darwin)    || defined(__darwin__) || defined(__darwin) ||\
       defined(__APPLE__) || defined(__apple__)  || defined (__DARWIN__)
   /* Apple MacOSX */
#  if !defined(OS_MACOS)
#   define OS_MACOS
#  endif
#  if !defined(OS_MACOSX)
#   define OS_MACOSX
#  endif
#  if !defined(OS_UNIX)
#   define OS_UNIX
#  endif
/*----------------------------------------------------------------------------*/
# endif /* SO macros */
/*----------------------------------------------------------------------------*/
/* platform macros (64bits) */
/*----------------------------------------------------------------------------*/
#  if defined(_WIN64) || defined(__LP64__) || \
      defined(__sparcv9) || defined(__64BIT__)
#   if !defined(OS_IS64BITS)
#    define OS_IS64BITS
#   endif
#  else
#   if !defined(OS_IS32BITS)
#    define OS_IS32BITS
#   endif
#  endif
/*----------------------------------------------------------------------------*/
/* config macros for library import / export */
/*----------------------------------------------------------------------------*/
/* DLIMPORT */
# ifndef DLIMPORT
#  ifdef OS_WINDOWS
#   define DLIMPORT __stdcall
#  else
#   define DLIMPORT
#  endif
# endif
/* DLEXPORT */
# ifndef DLEXPORT
#  ifdef OS_WINDOWS
#   define DLEXPORT extern "C" __stdcall __declspec(dllexport)
#  else
#   define DLEXPORT extern "C"
#  endif
# endif
/*----------------------------------------------------------------------------*/
#elif defined(CC_MSVC) /* MSVC */
/*----------------------------------------------------------------------------*/
#  if !defined(OS_WINDOWS)
#   define OS_WINDOWS
#  endif
/*----------------------------------------------------------------------------*/
/* platform macros (64bits) */
/*----------------------------------------------------------------------------*/
#  if defined(_WIN64)
#   if !defined(OS_IS64BITS)
#    define OS_IS64BITS
#   endif
#  else
#   if !defined(OS_IS32BITS)
#    define OS_IS32BITS
#   endif
#  endif
/*----------------------------------------------------------------------------*/
/* config macros for library import / export */
/*----------------------------------------------------------------------------*/
/* DLIMPORT */
# ifndef DLIMPORT
#   define DLIMPORT __declspec(dllimport)
# endif
/* DLEXPORT */
# ifndef DLEXPORT
#   define DLEXPORT extern "C" __declspec(dllexport)
# endif
/*----------------------------------------------------------------------------*/
#endif /* compiler specific */
/*----------------------------------------------------------------------------*/
/* MS Windows specific defines */
#if defined(OS_WINDOWS)
#  ifdef _SECURE_SCL
#   undef _SECURE_SCL
#  endif
#  define _SECURE_SCL 0 /* for disable MSVC iterator warnings */
#  ifdef _CRT_SECURE_NO_WARNINGS
#   undef _CRT_SECURE_NO_WARNINGS
#  endif
#  define _CRT_SECURE_NO_WARNINGS 1 /* for disable CRT secure warnings */
#endif
/*----------------------------------------------------------------------------*/
/** @endcond */

/*----------------------------------------------------------------------------*/
/* compatibility macros */

/** @cond SKIP_DOCUMENTATION */
#ifndef DLLIMPORT
# define DLLIMPORT DLIMPORT
#endif

#ifndef DLLEXPORT
# define DLLEXPORT DLEXPORT
#endif

#ifdef OS_WINDOWS
# ifndef WINDOWS
#  define WINDOWS
# endif
#endif

#ifdef OS_LINUX
# ifndef LINUX
#  define LINUX
# endif
#endif

#ifdef OS_FREEBSD
# ifndef FREEBSD
#  define FREEBSD
# endif
#endif

#ifdef OS_OPENBSD
# ifndef OPENBSD
#  define OPENBSD
# endif
#endif

#ifdef OS_NETBSD
# ifndef NETBSD
#  define NETBSD
# endif
#endif

#ifdef OS_SOLARIS
# ifndef SOLARIS
#  define SOLARIS
# endif
#endif

#ifdef OS_MACOSX
# ifndef MACOSX
#  define MACOSX
# endif
#endif

#ifdef OS_UNIX
# ifndef UNIX
#  define UNIX
# endif
#endif
/** @endcond */

/*----------------------------------------------------------------------------*/

#endif /* LIB_MCL_PLATFORM_HPP_ */

/*----------------------------------------------------------------------------*/
