/*----------------------------------------------------------------------------*/

#ifndef COMMON_BEANS_LOG_HPP_
#define COMMON_BEANS_LOG_HPP_ 1

/*----------------------------------------------------------------------------*/

#include "common/config.hpp"
#include "common/beans/device.hpp"

#include <ostream>

/*----------------------------------------------------------------------------*/

namespace supervise {

/*----------------------------------------------------------------------------*/

enum class LogEvent {
  /** Nenhum. */
  None = -1,
  /** Inicio de Monitoramento. */
  Start = 0,
  /** Fim de Monitoramento. */
  Stop = 1,
  /* falhas */
  /** Falha sobretemperatura. */
  Overtemp = 2,
  /** Falha interna. */
  Internal = 3,
  /** Falha fim de bateria. */
  EndBattery = 4,
  /** Falha sobrecarga. */
  Overload = 5,
  /** Falha curto-circuito na saida. */
  Shortcircuit = 6,
  /** Falha tensao de saida anormal. */
  AbnormalVOutput = 7,
  /** Falha tensao de bateria anormal. */
  AbnormalVBattery = 8,
  /** Falha inversor. */
  Inverter = 9,
  /** Falha rede muito baixa. */
  LoVInputFail = 51,
  /** Falha Potencia de Saida Muito Alta. */
  HiPOutputFail = 52,
  /** Falha no Tiristor. */
  Tiristor = 53,
  /* estados */
  /** Modo bateria. */
  OpBattery = 10,
  /** Modo rede/normal. */
  OpNormal = 11,
  /** Modo stand-by. */
  OpStandBy = 12,
  /** Modo Atencao. */
  OpWarning = 14,
  /** Modo Startup. */
  OpStartup = 15,
  /** Frequencia de entrada baixa. */
  LoFInput = 16,
  /** Frequencia de entrada alta. */
  HiFInput = 17,
  /** Rede eletrica normal. */
  NormalVInput = 18,
  /** Falha de sincronismo. */
  NoSyncInput = 19,
  /** Controle de Sincronismo OK. */
  SyncInput = 20,
  /** Tensao de entrada baixa. */
  LoVInput = 21,
  /** Tensao de entrada alta. */
  HiVInput = 22,
  /** Ausencia de rede. */
  NoVInput = 24,
  /** Bateria baixa. */
  LoBattery = 26,
  /** Bateria normal. */
  NormalBattery = 27,
  /** Ruido na rede. */
  NoiseInput = 28,
  /** Bateria totalmente carregada. */
  MaxBattery = 29,
  /** Contador regressivo do nobreak ativado. */
  ShutdownTimerActive = 30,
  /** Comunicando. */
  Connected = 31,
  /** Nao comunicando. */
  NoConnected = 32,
  /** Controle Remoto acionado. */
  RemoteControlActive = 33,
  /** Nao Calibrado. */
  NoCalibrated = 35,
  /** Descarga Completa de Bateria ativada. */
  OpCheckup = 48,
  /** Ventilador Anormal. */
  FanFail = 49,
  /** Indica que a bateria esta' ausente (desconectada). */
  NoBattery = 54,
  /** Indica que a bateria esta' envelhecida. */
  OldBattery = 55,
  /** Indica "Baixa Sobrecarga" na Saida (potencia em nivel critico). */
  HiPOutput = 56,
  /** Indica autonomia expandida. */
  MoreBattery = 57,
  /** Indica autonomia reduzida. */
  LessBattery = 58,
  /* shutdown */
  /** Shutdown por tempo autonomia. */
  ShutdownOpBattery = 36,
  /** Shutdown manual. */
  ShutdownManual = 37,
  /** Shutdown por bateria baixa. */
  ShutdownLoBattery = 38,
  /** Shutdown por falha/fim bateria. */
  ShutdownFail = 39,
  /** Shutdown por cont. regress. ativado/timer/agendado. */
  ShutdownTimer = 40,
  /* cancelamento de shutdown */
  /** Shutdown cancelado pelo usuario. */
  ShutdownCancelled = 50
};

/*----------------------------------------------------------------------------*/

std::ostream& operator<< (std::ostream& os, LogEvent src);

/*----------------------------------------------------------------------------*/

class LogEntry: public Object {
  public:
    StdString       id;
    LogEvent        event;
    LLong           dateTime;
    DeviceVars      vars;
    DeviceFlags     flags;
    DeviceFailFlags fail;
    LogEntry();
    LogEntry(const LogEntry& src);
    virtual ~LogEntry();
    virtual LogEntry& operator=(const LogEntry& src);
    virtual void clear(void);
    virtual StdString toString(void) const;
};

/*----------------------------------------------------------------------------*/

typedef std::vector<LogEntry> LogList;
typedef std::map<StdString,LogList> LogMap;

/*----------------------------------------------------------------------------*/

} /* namespace supervise */

/*----------------------------------------------------------------------------*/

#endif /* COMMON_BEANS_LOG_HPP_ */

/*----------------------------------------------------------------------------*/
