/*----------------------------------------------------------------------------*/

#include "log.hpp"
#include "mcl/core/string.hpp"
#include "mcl/core/datetime.hpp"

#include <sstream>

/*----------------------------------------------------------------------------*/

namespace supervise {

/*----------------------------------------------------------------------------*/

std::ostream& operator<< (std::ostream& os, LogEvent src) {
  /*
  switch (src) {
    case PortType::Unknown: return (os << "Unknown");
    case PortType::RS232  : return (os << "RS232"  );
    case PortType::HID    : return (os << "HID"    );
    case PortType::CDC    : return (os << "CDC"    );
  }
  */
  return (os << static_cast<Int>(src));
}

/*----------------------------------------------------------------------------*/

LogEntry::LogEntry(): Object() {
  clear();
}

LogEntry::LogEntry(const LogEntry& src): Object() {
  operator=(src);
}

LogEntry::~LogEntry() {}

LogEntry& LogEntry::operator=(const LogEntry& src) {
  this->vars      = src.vars     ;
  this->flags     = src.flags    ;
  this->fail      = src.fail     ;
  this->id        = src.id       ;
  this->dateTime  = src.dateTime ;
  this->event     = src.event    ;
  return (*this);
}

void LogEntry::clear(void) {
  vars     .clear();
  flags    .clear();
  fail     .clear();
  id       .clear();
  dateTime = 0LL;
  event    = LogEvent::None;
}

StdString LogEntry::toString(void) const {
  std::ostringstream result;
  result << "id=" << (id.empty()?"<unknown>":id) << ", ";
  if (dateTime > 0) {
    result << "dateTime=" << DateTime::dateTimeToStr(dateTime) << ", ";
  }
  if (event != LogEvent::None) {
    result << "event=" << event << ", ";
  }
  result << "vars: {"  << vars .toStr() << "}, ";
  result << "flags: {" << flags.toStr() << "}, ";
  result << "fail: {"  << fail .toStr() << "}";
  return (result.str());
}

/*----------------------------------------------------------------------------*/

} /* namespace supervise */

/*----------------------------------------------------------------------------*/
